import * as Location from 'expo-location';
import { Camera, CameraType } from "expo-camera";
import targets, { connect } from "./utils/socket";
import * as FaceDetector from "expo-face-detector";
import Communications from 'react-native-communications';
import React, { useState, useRef } from "react";
import {euclideanDistance, landMarks} from './utils/Recognition';
import { LogBox, SafeAreaView, Modal, TouchableOpacity, TouchableWithoutFeedback, Text, View, Image, Dimensions } from "react-native";
import OACam, { CaptureQuality } from 'react-native-openalpr';
import { StatusBar } from 'expo-status-bar';
import { useFonts } from 'expo-font';

export default function App() {
    LogBox.ignoreAllLogs();
	var [phone, setPhone] = useState("4244718820");
    var cameraRef = useRef(null);
    var [name, setName] = useState("");
    var [description, setDescription] = useState("");
    var [modalTargetFace, setModalTargetFace] = useState("https://www.fbi.gov/wanted/murders/robert-william-fisher/@@images/image/preview");
    var [modalDetectedFace, setModalDetectedFace] = useState("https://www.fbi.gov/wanted/murders/robert-william-fisher/@@images/image/preview");
    var [faceModalVisible, setFaceModalVisible] = useState(false);
    var [plateModalVisible, setPlateModalVisible] = useState(false);
    var [modalDetectedPlate, setModalDetectedPlate] = useState("https://www.fbi.gov/wanted/murders/robert-william-fisher/@@images/image/preview");
    var [detectedPlate, setDetectedPlate] = useState("https://www.fbi.gov/wanted/murders/robert-william-fisher/@@images/image/preview");
    var [permission, requestPermission] = Camera.useCameraPermissions();
    if(!permission?.granted) requestPermission();
    var [permission, requestPermission] = Location.useForegroundPermissions();
    if(!permission?.granted) requestPermission({accuracy: Location.LocationAccuracy.Lowest});
    var [which, setWhich] = useState(0);
    const [fontsLoaded] = useFonts({lexend: require("./assets/fonts/lexend/regular.ttf")});
    if(!fontsLoaded) return null;
    Location.getCurrentPositionAsync().then(location => connect(location.coords));

    function handleFacesDetected({faces}){
        console.log("detection");
        console.log(faceModalVisible);
        console.log(targets.length);
        if(plateModalVisible || faceModalVisible) return;
        for(var i = 0;i<faces.length;i++){
            var distances = targets.map((u) => ({...u, match: euclideanDistance(landMarks(faces[i]), u.landmarks)}));
            distances.sort((a, b) => a.match - b.match);
            console.log(distances);
            if(distances?.[0]?.match < 4){
                setFaceModalVisible(true);
                if(plateModalVisible || faceModalVisible) return;
                console.log("detected");
                setName(distances[0].name);
                if(distances[0].mw == "m"){
                    setDescription(`${distances[0].age} years old, Missing since ${distances[0].missing}`);
                    setPhone(distances[0].call)
                }
                if(distances[0].mw == "w"){
                    setDescription(`FBI wanted for ${distances[0].crime}`);
                    setPhone("911")
                }
                setModalTargetFace(distances[0].image);
                cameraRef.takePictureAsync({
                    exif: false,
                    skipProcessing: true
                }).then((picture) => setModalDetectedFace(picture.uri));
                break;
            }
        }
    }

    function handlePlateDetected({plate}){
        if(plateModalVisible || faceModalVisible) return;
        if(plate != "ABC1234") return;
        setPlateModalVisible(true);
        setDetectedPlate(plate);
        setWhich(0);
        var ref = cameraRef;
        setTimeout(function(){
            ref.takePictureAsync({
                exif: false,
                skipProcessing: true
            }).then((picture) => {
                setModalDetectedPlate(picture.uri);
                setWhich(1);
            });
        }, 100);
    }
    
    return (
		<SafeAreaView style={{
			flex: 1,
			backgroundColor: '#2E2E2E',
		}}>
			<View style={{
                backgroundColor: '#2E2E2E',
                width: '100%',
                justifyContent: 'center',
                padding: '5%',
                margin: 0,
            }}>
                <Text style={{color: 'white', fontSize: 20, fontFamily: 'lexend'}}>AARON <Text style={{color: 'white', fontSize: 15, fontFamily: 'lexend'}}>{which ? "License Plate Detection" : "Facial Recognition"}</Text></Text>
            </View>
			<TouchableWithoutFeedback onPress={() => setWhich(1-which)}>
                <View>
                    <Camera
                        ref={(ref) => {cameraRef = ref}}
                        type={CameraType.back}
                        style={{width: which ? "0%" : "100%", height: which ? "0%" :"100%"}}
                        faceDetectorSettings={{
                            mode: FaceDetector.FaceDetectorMode.accurate,
                            detectLandmarks: FaceDetector.FaceDetectorLandmarks.all,
                            runClassifications: FaceDetector.FaceDetectorClassifications.none,
                            minDetectionInterval: 300,
                            tracking: true
                        }}
                        onFacesDetected={handleFacesDetected}
                    />
                    { !!which &&
                        <OACam
                            ref={(ref) => {shotRef = ref}}
                            captureQuality={CaptureQuality.low}
                            style={{width: "100%", height: "100%"}}
                            country="us"
                            onPlateRecognized={handlePlateDetected}
                            showPlateOutline={false}
                            touchToFocus={false}
                        />
                    }
                </View>
            </TouchableWithoutFeedback>
            <Modal
                visible={plateModalVisible}
                animationType="slide">
                <View style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginTop: 22,
                    width: Dimensions.get('window').width
                }}>
                    <Text style={{fontSize: 30, margin: Dimensions.get('window').width*0.025}}>Detected Plate</Text>
                    <Image source={{uri: modalDetectedPlate}} style={{
                        width: Dimensions.get('window').width*0.8,
                        height: Dimensions.get('window').width*0.8,
                        margin: Dimensions.get('window').width*0.025
                    }}/>
                    <Text style={{fontSize: 20, margin: Dimensions.get('window').width*0.025}}>Does it say {detectedPlate}?</Text>
                    <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                        <TouchableOpacity onPress={() => {Communications.phonecall("911", true); setPlateModalVisible(false)}} style={{
                            backgroundColor: '#DDDDDD',
                            padding: 10,
                            margin: Dimensions.get('window').width*0.025
                        }}>
                            <Text style={{fontSize: 15}}>Confirm</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => setPlateModalVisible(false)} style={{
                            backgroundColor: '#DDDDDD',
                            padding: 10,
                            margin: Dimensions.get('window').width*0.025
                        }}>
                            <Text style={{fontSize: 15}}>Cancel</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
            <Modal
                visible={faceModalVisible}
                animationType="slide">
                <View style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginTop: 22,
                    width: Dimensions.get('window').width
                }}>
                    <Text style={{fontSize: 30, margin: Dimensions.get('window').width*0.025}}>Detected Face</Text>
                    <View style={{flexDirection: 'row', justifyContent: 'center',}}>
                        <Image source={{uri: modalDetectedFace}} style={{
                            width: Dimensions.get('window').width*0.4,
                            height: Dimensions.get('window').width*0.4,
                            margin: Dimensions.get('window').width*0.025
                        }}/>
                        <Image source={{uri: modalTargetFace}} style={{
                            width: Dimensions.get('window').width*0.4,
                            height: Dimensions.get('window').width*0.4,
                            margin: Dimensions.get('window').width*0.025
                        }}/>
                    </View>
                    <Text style={{lineHeight: 20,fontSize: 15, width: Dimensions.get('window').width*0.85, margin: Dimensions.get('window').width*0.025 }}>
                        <Text style={{fontSize: 20}}>{name}</Text>{'\n'}
                        {description}
                    </Text>
                    <View style={{flexDirection: 'row', justifyContent: 'center',}}>
                        <TouchableOpacity onPress={() => {Communications.phonecall(phone, true); setFaceModalVisible(false)}} style={{
                            backgroundColor: '#DDDDDD',
                            padding: 10,
                            margin: Dimensions.get('window').width*0.025
                        }}>
                            <Text style={{fontSize: 15}}>Confirm</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => setFaceModalVisible(false)} style={{
                            backgroundColor: '#DDDDDD',
                            padding: 10,
                            margin: Dimensions.get('window').width*0.025
                        }}>
                            <Text style={{fontSize: 15}}>Cancel</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
			<StatusBar style="light" />
		</SafeAreaView>
	);
}