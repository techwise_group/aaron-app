import React, { useState } from 'react';
import { Text, View, Modal, Pressable } from 'react-native';

export default function PopupView(){
    [isVisible, setIsVisible] = useState(false)
    
    return (
        <View style = {{flex : 1}}>
            <Modal 
                animationType="slide" 
                visible={isVisible}
                onRequestClose={() => {
                    setIsVisible(!isVisible)
                }}>
                    <View> 
                        <Text>Confirm if this is the same person as in the picture. This is text inside of the popup</Text>
                        <Pressable>
                            <Text>No. This is text inside of the button</Text>
                        </Pressable>
                        <Pressable>
                            <Text>Yes</Text>
                        </Pressable>
                    </View>
            </Modal>
        </View>
    );

}