import { Matrix, inverse, pseudoInverse } from 'ml-matrix';

export function euclideanDistance(f1, f2){
    var distance = 0;
    for(var i=0;i<f1.length;i++){
        distance += scalarEuclideanDistance(f1[i], f2[i])
    }
    return distance;
}

export function landMarks(face){
    var landmarks = [];
    for(key in face){
        if(!key.includes("Position")) continue;
        landmarks.push([(face[key].x-face.bounds.origin.x)/face.bounds.size.width, (face[key].y-face.bounds.origin.y)/face.bounds.size.height])
    }
    return landmarks;
}

function scalarEuclideanDistance(a, b){
    return Math.sqrt(a.map((_, i) => Math.pow(a[i]-b[i], 2)).reduce((partialSum, acc) => partialSum + acc, 0))
}

function unRotate(p, r){
    var a = r[0];
    var b = r[1];
    var y = r[2];
    var rotationMatrix = new Matrix([
        [Math.cos(a)*Math.cos(b), Math.cos(a)*Math.sin(b)*Math.sin(y)-Math.sin(a)*Math.cos(y), Math.cos(a)*Math.sin(b)*Math.cos(y)+Math.sin(a)*Math.sin(y)],
        [Math.sin(a)*Math.cos(b), Math.sin(a)*Math.sin(b)*Math.sin(y)+Math.cos(a)*Math.cos(y), Math.sin(a)*Math.sin(b)*Math.cos(y)-Math.cos(a)*Math.sin(y)],
        [-Math.sin(b), Math.cos(b)*Math.sin(y), Math.cos(b)*Math.cos(y)]
    ]);
    var pointMatrix = new Matrix([p]);
    var inv = inverse(rotationMatrix);
    var dist = pointMatrix.mmul(inv);
    return dist.data[0];
}