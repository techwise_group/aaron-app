import uuid from 'react-native-uuid';
import { io } from "socket.io-client";
import {landMarks} from './Recognition';
import * as FaceDetector from "expo-face-detector";
import { cacheDirectory, downloadAsync, deleteAsync } from "expo-file-system";

var socket;
var targets = [];
export default targets;
var remove = [];

export async function connect(coords){
    socket = io.connect("http://aaron.madum.cc", {query: {coords: `${coords.latitude},${coords.longitude}`}});
    socket.on("face", function(add, rem){
        console.log(add);
        remove.concat(rem);
        rem.forEach(function(i){
            targets = targets.filter((u) => u.id != i);
        });
        add.forEach(async function(person){
            var uri = await downloadAsync(person.image, cacheDirectory + uuid.v4());
            FaceDetector.detectFacesAsync(uri.uri, {
                mode: FaceDetector.FaceDetectorMode.accurate,
                detectLandmarks: FaceDetector.FaceDetectorLandmarks.all,
                runClassifications: FaceDetector.FaceDetectorClassifications.none,
                minDetectionInterval: 100,
            }).then(async function({faces}){
                await deleteAsync(uri.uri);
                if(!faces.length) return;
                if(remove.includes(person.id)) return;
                person.landmarks = landMarks(faces[0]);
                targets.push(person);
            }).catch(function(err){
                console.error(err);
            });
        });
    });
};